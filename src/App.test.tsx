import React from 'react';
import { render, act, fireEvent, waitForDomChange } from '@testing-library/react';
import App from './App';

describe('app', () => {
    it('renders', () => {
        const page = act(() => {
            render(<App />);
            
            return Promise.resolve();
        }); 

        expect(page).toBeTruthy();
    });

    it('deletes item', () => {
        const page = act(async () => {
            const { getByText, getByRole } = render(<App />);

            await waitForDomChange();

            fireEvent.click(getByText('Delete'));
            fireEvent.click(getByRole('modal-close')); 

            return Promise.resolve();
        }); 

        expect(page).toBeTruthy();
    });  
});

