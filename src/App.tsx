import React from 'react';

//ROUTER
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom';

// LOGO
import { logo } from './assets';

// STYLES
import './App.scss';

// CONTAINERS
import { 
  Home, 
  NewItem, 
  UpdateItem, 
  DetailItem 
} from './components/templates';

const App = () => (
  <Router>
    <nav className="navbar is-dark is-fixed-top" role="navigation" aria-label="main navigation">
      <div className="navbar-brand">
        <Link className="navbar-item" to="/">
          <img src={logo} alt="wefox_logo" width="112" height="28" />
        </ Link>
      </div>
    </nav>
    <div className="content">
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route exact path="/item/new">
          <NewItem />
        </Route>
        <Route exact path="/item/edit/:itemId">
          <UpdateItem />
        </Route>
        <Route exact path="/item/:itemId">
          <DetailItem />
        </Route>
      </Switch>
    </div>
  </Router>
);

export default App;
