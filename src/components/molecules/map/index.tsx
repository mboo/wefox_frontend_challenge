import React from 'react';
import './styles.scss';
import { params } from './types';
 
const Map = ({ latitude, longitude }: params) => {
  return (
    <iframe 
      title="map"
      src={`https://maps.google.com/maps?q=${latitude},${longitude}&hl=es&z=14&output=embed`}
    />
  );
};
 
export default Map;