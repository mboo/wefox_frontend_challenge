const FIELDS_TO_AVOID = ['lat', 'long', 'image_url'];

export { FIELDS_TO_AVOID };