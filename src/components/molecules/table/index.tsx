import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

// CONSTANTS
import { FIELDS_TO_AVOID } from './constants';

import './styles.scss';
import { params } from './types';

const Table = ({ header, body, deleteItem }: params) => {
  const [composedHead, setComposedHead] = useState();
  const [composedBody, setComposedBody] = useState();

  const composeHead = (header: any) => {
    const headerComposition = header.map((el: string, i: number) => {
      return (FIELDS_TO_AVOID.includes(el))? null : <th key={i}>{ el }</th>;
    });
  
    headerComposition.push(<th key="actions">Actions</th>);
  
    return <tr>{ headerComposition }</tr>;
  };
  
  const composeBody = (body: any) => {
    const composeRows = (row: any, i: number) => {
      const response = (Object.keys(row) as Array<keyof typeof row>).map((el: any, j: any) => {
        return (FIELDS_TO_AVOID.includes(el))? null : <td key={j}>{ row[el] }</td>;
      });
  
      response.push(<td className="actions" key={`actions${i}`}>
        <Link to={`item/${row.id}`} className="button is-success">Details</Link>
        <Link to={`item/edit/${row.id}`} className="button is-link">Edit</Link>
        <button onClick={() => deleteItem(row.id)} className="button is-danger">Delete</button>
        </td>
      );
  
      return response;
    };
  
    return body.map((row: Object, i: number) => 
      <tr key={i}>{ composeRows(row, i) }</tr>);
  };
  
  // compose header
  useEffect(() => {
    if (header) setComposedHead(composeHead(header));
  },[header]);

  // compose body
  useEffect(() => {
    if (body) setComposedBody(composeBody(body));
  },[body]);

    return (
      <div className="table-container">
        <table className="table is-striped is-hoverable">
          <thead>
            { composedHead }
          </thead>
          <tbody>
            { composedBody }
          </tbody>
        </table>
      </div>
    );
};

export default Table;