import React from 'react';
import { params } from './types';

const Card = ({ image_url, title, content }: params) => {
  return (
    <div className="card">
      <div className="card-image">
        <figure className="image">
          <img src={ image_url } alt={ title } />
        </figure>
      </div>
      <div className="card-content">
        <div className="media">
          <div className="media-content">
            <p className="title is-4">{ title }</p>
          </div>
        </div>
        <div className="content">
        { content }
        </div>
      </div>
    </div>
  );
}

export default Card;