export type params = { 
    image_url: string, 
    title: string,
    content: string 
};