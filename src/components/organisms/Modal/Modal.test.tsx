import React from 'react';
import { render } from '@testing-library/react';
import Modal from './index';

const callback = jest.fn();

describe('New Item test suite',() => {
    it('renders', () => {
        const page = render(<Modal isActive={true} body='test' callback={callback} />);
        expect(page).toBeTruthy();
    });
});
