import React, { useEffect, useState } from 'react';
import { params } from './types';

const Modal = ({ isActive, body, callback }: params) => {
  const [content, setContent] = useState('');
  const [open, setOpen] = useState(false);

  // Lifecycle method for isActive
  useEffect(() => setOpen(isActive), [isActive]);

  // Lifecycle method for content
  useEffect(() => setContent(body), [body]);

  return (
    <div className={`modal ${(open)? 'is-active' : ''}`}>
      <div className="modal-background" />
      <div className="modal-content">
        <div className="box">
          { content }
        </div>
      </div>
      <button className="modal-close is-large" role="modal-close" onClick={callback}/>
    </div>
  );
}

export default Modal;