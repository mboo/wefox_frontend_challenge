import React from 'react';
import { render, act } from '@testing-library/react';
import DetailItem from './index';

describe('New Item test suite', () => {
    it('renders', () => {
        const page = act(() => {
            render(<DetailItem />)

            return Promise.resolve();
        });

        expect(page).toBeTruthy();
    });
});
