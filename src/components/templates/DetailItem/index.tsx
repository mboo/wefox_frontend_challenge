import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

// CONSTANTS
import { DEFAULT_FIELDS } from './constants';

// API
import { show } from '../../../service';

// ORGANISMS
import { Card } from '../../organisms';

// MOLECULES
import { Map } from '../../molecules';

const DetailItem = () => {
  const [fields, setFields] = useState(DEFAULT_FIELDS);

  const { itemId } = useParams();

  const fetchSingleItem = async (itemId: string | undefined) => {
    const allData: any = await show(itemId);
    setFields(allData);
  };

  useEffect(() => {
    fetchSingleItem(itemId);
  },[itemId]);

  return (
    <div className="section content">
      <div className="columns">
        <div className="column">
          <Card 
            content={fields.content} 
            title={fields.title} 
            image_url={fields.image_url}
          />
        </div>
        <div className="column">
          <Map latitude={fields.lat} longitude={fields.long} />
        </div>
      </div>
    </div>
  );
};

export default DetailItem;