import React, { useEffect, useState, useCallback } from 'react';
import { Link } from 'react-router-dom';

// API
import { list } from '../../../service';
import { remove } from '../../../service';

// MOLECULES
import { Table } from '../../molecules';

// ORGANISMS
import { Modal } from '../../organisms';

const Home = () => {
  const [allData, setAllData] = useState();
  const [tableHeader, setTableHeader] = useState();

  // modal states
  const [requestStatus, setRequestStatus] = useState('');
  const [isActive, setIsActive] = useState(false);

  const callback = useCallback(() => {
    setIsActive(false);
  }, []);

  const fetchAllData = async () => {
    const allData: any = await list();
    setAllData(allData);
    setTableHeader(Object.keys(allData[0]));
  };

  const deleteItem = async (id: string) => {

    const response = await remove(id);

    setIsActive(true);
    setRequestStatus('Something has been gone wrong... the item was not deleted');
    
    if (response) {
      await fetchAllData();
      setIsActive(true);
      setRequestStatus('Item succesfully deleted');
    }
  }

  // Use effect on Mount Lifecycle method
  useEffect(() => {
    fetchAllData();
  },[]);

  return (
    <div>
      <section className="section container is-fluid">
        <Link to="/item/new" className="button is-pulled-right is-dark">Add new item</Link>
      </section>
      <section className="container is-fluid">
        <Table header={tableHeader} body={allData} deleteItem={deleteItem} />
      </section>
      <Modal callback={callback} isActive={isActive} body={requestStatus} />
    </div>
  );
};

export default Home;