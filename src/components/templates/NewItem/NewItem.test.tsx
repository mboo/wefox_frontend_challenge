import React from 'react';
import { render, act, fireEvent } from '@testing-library/react';
import NewItem from './index';

describe('New Item test suite',() => {
    it('renders', () => {
        const page = act(async () => {
            render(<NewItem />);

            return await Promise.resolve();
        });

        expect(page).toBeTruthy();
    });

    it('submits form and returns ok', () => {
        const page = act(async () => {
            const { getByText, getByRole } = render(<NewItem />);

            fireEvent.click(getByText('Submit new Item'));
            fireEvent.click(getByRole('modal-close')); 

            return await Promise.resolve();
        });

        expect(page).toBeTruthy();
    });

    it('submits form and returns not-ok', () => {
        const page = act(async () => {
            const { getByText } = render(<NewItem />);

            fireEvent.click(getByText('Submit new Item'));

            return await Promise.resolve();
        });

        expect(page).toBeTruthy();
        
    });
});
