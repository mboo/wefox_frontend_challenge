import React, { useState, useCallback } from 'react';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';

// API
import { create } from '../../../service';

// ORGANISMS
import { Modal } from '../../organisms';

const NewItem = () => {
  const { register, handleSubmit } = useForm();
  const history = useHistory();

  // modal states
  const [requestStatus, setRequestStatus] = useState('');
  const [isActive, setIsActive] = useState(false);

  const callback = useCallback(() => {
    setIsActive(false);
    history.push('/');
  }, []);
  
  const onSubmit = async (data: RequestInit) => {
    const response = await create(data);
    setIsActive(true);
    setRequestStatus('Woops, an error make the creation fail');

    if(response) setRequestStatus('Item created successfully');
  }
  
  return (
    <div className="section is-fluid">
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="field">
          <label className="label">Title (*)</label>
          <div className="control">
            <input name="title" ref={register} className="input" required type="text" placeholder="Write a title" />
          </div>
          <p className="help">Try to be concise!</p>
        </div>
        <div className="field">
          <label className="label">Image URL</label>
          <div className="control">
            <input name="image_url" ref={register} className="input" type="text" placeholder="Paste the image Url" />
          </div>
          <p className="help">It needs to be an absolute link, otherwise is not going to work...</p>
        </div>
        <div className="field">
          <label className="label">Description (*)</label>
          <div className="control">
            <textarea name="content" ref={register} className="textarea" required placeholder="Description of the item"></textarea>
          </div>
        </div>
        <div className="columns">
          <div className="field column">
            <label className="label">Latitude</label>
            <div className="control">
              <input name="lat" ref={register} className="input" type="text" placeholder="Paste the image Url" />
            </div>
          </div>
          <div className="field column">
            <label className="label">Longitude</label>
            <div className="control">
              <input name="long" ref={register} className="input" type="text" placeholder="Paste the image Url" />
            </div>
          </div>
        </div>
        <div className="control">
          <button className="button is-primary is-pulled-right">Submit new Item</button>
        </div>
      </form>
      <Modal callback={callback} isActive={isActive} body={requestStatus} />
    </div>
  );
};

export default NewItem;