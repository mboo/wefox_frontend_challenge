import React from 'react';
import { render, act, fireEvent } from '@testing-library/react';
import UpdateItem from './index';

describe('Update Item test suite',() => {
    it('renders', () => {
        const page = act(async () => {
            render(<UpdateItem />);

            return await Promise.resolve();
        });

        expect(page).toBeTruthy();
    });

    it('clicks submit button', () => {
        const page = act(async () => {
            const { getByText, getByRole } = render(<UpdateItem />);

            fireEvent.click(getByText('Update Item')); 
            fireEvent.click(getByRole('modal-close')); 

            expect(getByText('Update Item')).toBeTruthy();

            return await Promise.resolve();
        });

        expect(page).toBeTruthy();
    });
});


