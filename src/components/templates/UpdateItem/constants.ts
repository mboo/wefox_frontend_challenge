const DEFAULT_FIELDS = {
    title: '',
    content: '',
    lat: '',
    long: '',
    image_url: ''
};

export { DEFAULT_FIELDS };