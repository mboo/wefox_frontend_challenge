import React, { useEffect, useState, useCallback } from 'react';
import { useForm } from 'react-hook-form';
import { useParams } from 'react-router-dom';

// CONSTANTS
import { DEFAULT_FIELDS } from './constants';

// API
import { show, update } from '../../../service';

// MOLECULES
import { Modal } from '../../organisms';

const UpdateItem = () => {
  const { register, handleSubmit, watch, errors } = useForm();

  const [fields, setFields] = useState(DEFAULT_FIELDS);
  
  // modal states
  const [requestStatus, setRequestStatus] = useState('');
  const [isActive, setIsActive] = useState(false);

  const { itemId } = useParams();

  const fetchSingleItem = async (itemId: string | undefined) => {
    const allData: any = await show(itemId);
    setFields(allData);
  };

  const callback = useCallback(() => {
    setIsActive(false);
  }, []);

  const onSubmit = async (data: RequestInit) => {
    const response = await update(itemId, data);
    setIsActive(true);

    setRequestStatus('Woops, an error make the update fail');

    if(response) setRequestStatus('Item updated successfully');
  };

  useEffect(() => {
    fetchSingleItem(itemId)
  }, [itemId]);

  return (
    <div className="section is-fluid">
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="field">
          <label className="label">Title (*)</label>
          <div className="control">
            <input 
              name="title" 
              ref={register} 
              className="input" 
              required 
              type="text" 
              placeholder="Write a title"
              defaultValue={fields.title} 
            />
          </div>
          <p className="help">Try to be concise!</p>
        </div>
        <div className="field">
          <label className="label">Image URL</label>
          <div className="control">
            <input 
              name="image_url" 
              ref={register} 
              className="input" 
              type="text" 
              placeholder="Paste the image Url"
              defaultValue={fields.image_url} 
            />
          </div>
          <p className="help">It needs to be an absolute link, otherwise is not going to work...</p>
        </div>
        <div className="field">
          <label className="label">Description (*)</label>
          <div className="control">
            <textarea 
              name="content" 
              ref={register} 
              className="textarea" 
              required 
              placeholder="Description of the item"
              defaultValue={fields.content}
            />
          </div>
        </div>
        <div className="columns">
          <div className="field column">
            <label className="label">Latitude</label>
            <div className="control">
              <input 
                name="lat" 
                ref={register} 
                className="input" 
                type="text" 
                placeholder="Paste the image Url"
                defaultValue={fields.lat}
              />
            </div>
          </div>
          <div className="field column">
            <label className="label">Longitude</label>
            <div className="control">
              <input 
                name="long" 
                ref={register} 
                className="input" 
                type="text" 
                placeholder="Paste the image Url" 
                defaultValue={fields.long}
              />
            </div>
          </div>
        </div>
        <div className="control">
          <button className="button is-primary is-pulled-right">Update Item</button>
        </div>
      </form>
      <Modal body={requestStatus} isActive={isActive} callback={callback}/>
    </div>
  );
};

export default UpdateItem;