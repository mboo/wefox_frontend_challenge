const BASE_URL = 'https://wf-challenge-7sibao3pxp.herokuapp.com';
const ENDPOINT = '/api/v1/posts';
const URL = BASE_URL+ENDPOINT;

export { BASE_URL, ENDPOINT, URL };