import { list, show, create, update, remove } from './methods';
import {BASE_URL, ENDPOINT, URL} from './constants';

export { 
    list, 
    show, 
    create, 
    update, 
    remove,
    BASE_URL, 
    ENDPOINT, 
    URL 
};