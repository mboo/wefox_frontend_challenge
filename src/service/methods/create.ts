import { URL } from '../constants';

const create = async (fetchData: RequestInit): Promise<Object> => {
  const data = {
    method: 'POST',
    body: JSON.stringify(fetchData),
    headers: {
      'Content-Type': 'application/json'
    }
  };
    
  return fetch(URL, data).then(response => {
    if (!response.ok) throw new Error(response.statusText);

    return true;
  });
};

export default create;