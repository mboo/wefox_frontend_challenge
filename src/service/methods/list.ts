import { URL } from '../constants';

const list = async (): Promise<Object> => {
    return fetch(URL).then(response => {
        if (!response.ok) throw new Error(response.statusText);

        return response.json();
    });
};

export default list;