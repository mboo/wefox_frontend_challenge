import { URL } from '../constants';

const remove = async (itemId: string = '1', fetchData: RequestInit = {}): Promise<Object> => {
    
    fetchData.method = 'DELETE';
    
    return fetch(`${URL}/${itemId}`, fetchData).then(response => {
        if (!response.ok) throw new Error(response.statusText);

        return response.ok;
    });
};

export default remove;