import { URL } from '../constants';

const show = async (itemId: string = '1'): Promise<Object> => {
    return fetch(`${URL}/${itemId}`).then(response => {
        if (!response.ok) throw new Error(response.statusText);

        return response.json();
    });
};

export default show;