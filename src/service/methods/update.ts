import { URL } from '../constants';

const update = async (itemId: string = '1', fetchData: RequestInit): Promise<Object> => {
  const data = {
    method: 'PUT',
    body: JSON.stringify(fetchData),
    headers: {
      'Content-Type': 'application/json'
    }
  };
    
  return fetch(`${URL}/${itemId}`, data).then(response => {
    if (!response.ok) throw new Error(response.statusText);
        
    return true;
  });
};

export default update;