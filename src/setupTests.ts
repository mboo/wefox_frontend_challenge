// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
import '@testing-library/jest-dom/extend-expect';
import 'mutationobserver-shim';

// mocks
jest.mock('react-router-dom', () => ({
    ...jest.requireActual('react-router-dom'), // use actual for all non-hook parts
    useParams: () => ({
        itemId: '1',
    }),
    useRouteMatch: () => ({ url: '/company/company-id1/team/team-id1' }),
    useHistory: () => ({
        push: jest.fn()
    }),
}));

jest.mock('./service', () => ({
    show: () => ({
        title: 'madrid',
        content: 'madrid',
        lat: '1',
        long: '1',
        image_url: 'https://static.vueling.com/cms/media/1216342/valencia_sem.jpg'
    }),
    list: () => ([{
        title: 'madrid',
        content: 'madrid',
        lat: '1',
        long: '1',
        image_url: 'https://static.vueling.com/cms/media/1216342/valencia_sem.jpg'
    }]),
    create: () => (true),
    remove: () => (true),
    update: () => (true)
}));